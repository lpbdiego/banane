using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackPoints : MonoBehaviour
{
    [SerializeField] private int attackPoints = 1;
    public int getAttackPoints() {return attackPoints;}
    public void setAttackPoints(int val) {attackPoints = val;}
}
