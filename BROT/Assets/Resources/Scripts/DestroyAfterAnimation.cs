using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterAnimation : MonoBehaviour
{
    Animator anim;
    void Start() {
        anim = this.GetComponentInChildren<Animator>();
    }
        void Update()
    {
        if(anim.GetCurrentAnimatorStateInfo(0).IsName("Destroy")) {
            Destroy(this.gameObject);
        }
    }
}
