using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Controller : MonoBehaviour
{
    private Rigidbody2D rb2D;
    public float moveSpeed = 3f; 
    public float sprintSpeed = 5f;   
    public float Energy = 100f;
    bool dashEnabled = true;

    UIController UIController;
    
    
    // Start is called before the first frame update
    void Start()
    {
        this.rb2D = this.GetComponent<Rigidbody2D>();

        UIController = GameObject.FindGameObjectWithTag("UIController").GetComponent<UIController>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate(){

        if (Energy < 100f){
            Energy += 0.1f;
            UIController.UpdateEnergyBar(Energy);

            if (Energy >= 10f) {
                dashEnabled = true;
            }
        }



        Vector2 PlayerInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).normalized;

        float newSpeed = moveSpeed;

        if (Input.GetKey(KeyCode.LeftShift) && Energy > 0 && dashEnabled == true) {
            newSpeed = sprintSpeed;
            Energy -= 0.8f;
            UIController.UpdateEnergyBar(Energy);
            UIController.ShowSprintOverlay();


            if (Energy <= 0) {
                dashEnabled = false;

            }
            


            if(GameObject.Find("DevTools").GetComponent<DevTools>().getDevmode()){
                newSpeed  = sprintSpeed * 1.5f;
                Energy = 100f;
            }

        }

        Vector2 moveForce = PlayerInput*newSpeed;
        rb2D.velocity = moveForce;

    }
}
