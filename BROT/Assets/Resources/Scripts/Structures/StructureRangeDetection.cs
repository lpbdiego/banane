using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StructureRangeDetection : MonoBehaviour
{
    Collider2D cld;
    // Start is called before the first frame update
    void Start()
    {
        cld = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Collider2D getCollider() {
        return cld;
    }
}
