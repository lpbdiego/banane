using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;

public class EventClickPlaceables : MonoBehaviour, IPointerMoveHandler, IPointerDownHandler, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    public bool moving = false;
    private Vector3 newPos;
    private Vector3 previousPos;
    private float previousZ;
    private Collider2D collision;
    public static bool moveMode = false;
    private GameObject placeController;

    public void setController(GameObject placeController)
    {
        this.placeController = placeController;
        collision = GetComponent<Collider2D>();
    }
    public void OnPointerDown(PointerEventData eventData)
    {

    }

    public void OnPointerUp(PointerEventData eventData)
    {

    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (moveMode && eventData.button == PointerEventData.InputButton.Right)
        {
            moving = !moving;
            if (moving)
            {
                previousPos = transform.position;
                previousZ = previousPos.z;
            }
            else
            {
                placeController.GetComponent<PlacingController>().reposition(this.gameObject);
                this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, previousZ);
                collision.isTrigger = false;
            }
        
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        getNewPosition(eventData);
    }

    void getNewPosition(PointerEventData eventData)
    {
        if (moving)
        {
            collision.isTrigger = true;

            newPos = eventData.pointerCurrentRaycast.worldPosition;
            newPos.y += 0.2f;
            //newPos.z = -5f;
            this.transform.position = newPos;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {

        getNewPosition(eventData);
    }

    public void OnPointerMove(PointerEventData eventData)
    {
    }

    public void Update()
    {
        if (moving)
        {
            Vector3 screenPosition = Input.mousePosition;
            screenPosition.z = Camera.main.nearClipPlane + 1;
            Vector3 worldPosition = Camera.main.ScreenToWorldPoint(screenPosition);
            transform.position = worldPosition;
            transform.position = new Vector3(transform.position.x, transform.position.y + 0.2f, 0.0f);
        }
    }
}
