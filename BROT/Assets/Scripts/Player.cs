using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
/*
*@authors: Kai, Diego, Joshua
*/
public class Player : MonoBehaviour
{

    public float health, maxHealth = 1000;
    public float scrap = 0;
    UIController UIController;
    public SpriteRenderer godColor;
        SoundEffect soundEffects;
        SoundEffectDeathsounds soundEffectDeaths;



    // Start is called before the first frame update
    void Start()
    {

        health = maxHealth;     
        
        soundEffects = GetComponentInChildren<SoundEffect>();
        soundEffectDeaths = GetComponentInChildren<SoundEffectDeathsounds>();


        UIController = GameObject.FindGameObjectWithTag("UIController").GetComponent<UIController>();
        UIController.UpdateHealthCounter(health);

        godColor = GameObject.FindGameObjectWithTag("PlayerVisuals").GetComponent<SpriteRenderer>();

    }

    // Update is called once per frame
    void Update()
    {

    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Electricity"))
        {

            //TakeDamage(other.gameObject.GetComponent<AttackPoints>().getAttackPoints());
        }

        if (other.gameObject.CompareTag("Boom")) 
        {
            float distance = Mathf.RoundToInt(Vector3.Distance(this.transform.position, other.transform.position)*100);
            distance = distance == 0.0f ? 1.0f : distance;


           // Debug.Log(Mathf.RoundToInt((other.gameObject.GetComponent<AttackPoints>().getAttackPoints()) / distance));
            TakeDamage(Mathf.RoundToInt((other.gameObject.GetComponent<AttackPoints>().getAttackPoints()) / distance)*100);
        }

        //collect scrap
        if (other.gameObject.CompareTag("Scrap"))
        {
            soundEffects.SetCurrentSound(SoundEffect.SOUND_EFFECT.PICKUP);
            //soundEffects.changeVolume(2.0f);
            soundEffects.triggerSound();
            scrap++;
            //Debug.Log(scrap + " scrap");
            UIController.UpdateScrapCounter(scrap);

            Destroy(other.gameObject);

        }

        //collect hearts <3
        if (other.gameObject.CompareTag("Heart"))
        {
            soundEffects.SetCurrentSound(SoundEffect.SOUND_EFFECT.PICKUP);
            //soundEffects.changeVolume(2.0f);
            soundEffects.triggerSound();

            health += 150;
            if (health > 1000) {
                health = 1000;
            }
            UIController.UpdateHealthCounter(health);


            Destroy(other.gameObject);

        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(health);
        if (collision.gameObject.CompareTag("Enemy"))
        {
            TakeDamage(collision.gameObject.GetComponent<AttackPoints>().getAttackPoints());
        }

        /*
        if (collision.gameObject.CompareTag("Bullet"))
        {
            TakeDamage(collision.gameObject.GetComponent<AttackPoints>().getAttackPoints());
        }
        */
    }

    public void TakeDamage(int Damage)
    {

        soundEffects.SetCurrentSound(SoundEffect.SOUND_EFFECT.HIT);
        //soundEffects.changeVolume(0.5f);
        soundEffects.triggerSound();
        
        health -= Damage;

        //Call UI Damage Effect Here
        UIController.DamageOverlay();

        //Update UI
        UIController.UpdateHealthCounter(health);

        // Die if health goes below 0
        if (health <= 0)
        {
            if (!GameObject.Find("DevTools").GetComponent<DevTools>().getDevmode())
            {
                Death();
            }
        }


    }

    public void Death()
    {
        soundEffectDeaths.MakeDeathSound(SoundEffectDeathsounds.SOUND_EFFECT_DEATHS.PLAYER, .3f);

        SceneManager.LoadScene(2);
    }


    public void God()
    {

        if (GameObject.Find("DevTools").GetComponent<DevTools>().getDevmode())
        {
            godColor.color = new Color(.5f, 1f, 1f, 1f);
        }
        else
        {
            godColor.color = new Color(1f, 1f, 1f, 1f);
        }
    }



}
