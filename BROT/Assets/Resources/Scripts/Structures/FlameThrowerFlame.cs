using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameThrowerFlame : MonoBehaviour
{

    Transform ownerPosition;
    float dist;

    void setOwnerPosition(Transform owner) { ownerPosition = owner; }
    void setFlameDistance(float dist) { this.dist = dist; }
    public void setByOwner(Transform owner, float dist) { ownerPosition = owner; this.dist = dist; }

    void Update()
    {
        if (ownerPosition != null)
        {
            // Debug.Log(Vector3.Distance(ownerPosition.position, this.transform.position)*100 +">"+ dist);
            if (Vector3.Distance(ownerPosition.position, this.transform.position) > dist)
            {
                Destroy(this.gameObject);
            }
        }
    }
}
