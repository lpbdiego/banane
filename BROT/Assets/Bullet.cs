using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour //By Diego
{
    public GameObject explosionEffect;
    public GameObject bigExplosionEffect;
    public float mul = 2;

    private bool boosted = false;

    public GameObject electrifiedTexture;
    public GameObject defaultTexture;


    private bool ready;
    public float activationDelay = 0.1f;

    public CircleCollider2D circleCollider;
    SoundEffect soundEffect;
    SoundEffectDeathsounds soundEffectDeaths;

    private void Start()
    {        
        soundEffect = GetComponentInChildren<SoundEffect>();
        soundEffectDeaths = GetComponentInChildren<SoundEffectDeathsounds>();
        circleCollider.enabled = false;
        ready = false;
        StartCoroutine("PrepareBullet");
        
    }

    // Update is called once per frame
    void Update()
    {
        soundEffectDeaths.SetCurrentSound(SoundEffectDeathsounds.SOUND_EFFECT_DEATHS.BULLET);
        //soundEffect.changeVolume(.2f);
        //soundEffect.triggerSound();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Destroy itself when colliding with anything
        if (boosted)
        {
            GameObject Explosion = Instantiate(bigExplosionEffect, transform.position, Quaternion.identity);
            Explosion.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f) + this.transform.localScale * 3;
            Explosion.GetComponent<AttackPoints>().setAttackPoints(this.GetComponent<AttackPoints>().getAttackPoints() - 1);

        } else
        {
            GameObject Explosion = Instantiate(explosionEffect, transform.position, Quaternion.identity);
            Explosion.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f) + this.transform.localScale * 3;
            Explosion.GetComponent<AttackPoints>().setAttackPoints(this.GetComponent<AttackPoints>().getAttackPoints() - 1);

        }

  
       soundEffectDeaths.MakeDeathSound(SoundEffectDeathsounds.SOUND_EFFECT_DEATHS.BULLET, 1.0f);

        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Electricity")){
            Boost();
        }
    }

    private void Boost()
    {
        boosted = true;
        electrifiedTexture.SetActive(true);
        defaultTexture.SetActive(false);

     /*
    // this was the code that made the bullets bigger
        if (mul >= 1){
            mul -= 0.2f;
        }
        //Make bigger

        transform.localScale = transform.localScale * mul;
    */

    }
    


    private IEnumerator PrepareBullet()
    {
        yield return new WaitForSeconds(activationDelay);
        circleCollider.enabled = true;
        ready = true;
    }

    public bool isBoosted() {return boosted;}

 
}
