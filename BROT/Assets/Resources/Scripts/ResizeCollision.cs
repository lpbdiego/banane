using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ResizeCollision : MonoBehaviour
{
    [SerializeField] private float radius = 1.0f;
    new CircleCollider2D collider;
    void Start()
    {
        collider = this.transform.GetComponent<CircleCollider2D>();
        collider.radius = radius;
    }

    public void resize()
    {
        collider.radius = radius;
    }
    public void setRadius(float rad)
    {
        radius = rad;
        gameObject.GetComponentInChildren<SpriteRenderer>().transform.localScale *= radius * 2;
    }

}
