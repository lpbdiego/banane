using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Nexus : MonoBehaviour
{
    private int health;
    public int maxHealth;

    UIController UIController;
    SoundEffect soundEffects;
    SoundEffectDeathsounds soundEffectDeaths;
    SpriteRenderer[] allSpriteRenderers;
    SpriteRenderer enemySpriteRenderer = null;

    public SpriteRenderer damageColor;
    bool damageColorOn = false;
    float damageColorCounter = 0;

    // Start is called before the first frame update
    void Start()
    {
        UIController = GameObject.FindGameObjectWithTag("UIController").GetComponent<UIController>();
        
        health = maxHealth;
        soundEffects = GetComponentInChildren<SoundEffect>();
        soundEffectDeaths = GetComponentInChildren<SoundEffectDeathsounds>();

        allSpriteRenderers = GetComponentsInChildren<SpriteRenderer>();

            foreach (var sr in allSpriteRenderers)
            {
                if (sr.gameObject.name == "NexusVisuals")
                {
                    enemySpriteRenderer = sr;
                    break;
                }
            }

    }

    // Update is called once per frame
    void Update()
    {
        
        if (damageColorOn == true){

            damageColorCounter += Time.deltaTime * 300;

            if(damageColorCounter >= 0) {

                enemySpriteRenderer.color = new Color(1f, 1f - damageColorCounter * 0.01f, 1f - damageColorCounter * 0.01f, 1f);

            }

            if(damageColorCounter >= 50) {

                enemySpriteRenderer.color = new Color(1f, 0.5f  + (damageColorCounter - 50) * 0.01f, 0.5f  + (damageColorCounter - 50) * 0.01f, 1f);

            }

            if(damageColorCounter >= 100) {
                damageColorOn = false;
                damageColorCounter = 0;
            }
        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(health);
        if (collision.gameObject.CompareTag("Enemy"))
        {
            //TakeDamage(collision.gameObject.GetComponent<AttackPoints>().getAttackPoints());
            TakeDamage(1);
            Debug.Log("Nexus remaining: "+health);
            
        }

        
        float distance = Mathf.RoundToInt(Vector3.Distance(this.transform.position, collision.transform.position)*100);
            distance = distance == 0.0f ? 1.0f : distance;


           // Debug.Log(Mathf.RoundToInt((other.gameObject.GetComponent<AttackPoints>().getAttackPoints()) / distance));
           // TakeDamage(Mathf.RoundToInt((collision.gameObject.GetComponent<AttackPoints>().getAttackPoints()) / distance)*100);
    }

   
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            //TakeDamage(collision.gameObject.GetComponent<AttackPoints>().getAttackPoints());
            TakeDamage(1);
            Debug.Log("Nexus remaining: "+health);
        }
        if (other.gameObject.CompareTag("Boom")) 
        {
            float distance = Vector3.Distance(this.transform.position, other.transform.position);
            distance = distance == 0.0f ? 1.0f : distance;
            //TakeDamage(Mathf.RoundToInt(distance / other.gameObject.GetComponent<AttackPoints>().getAttackPoints()));
            TakeDamage(10);
            Debug.Log("Nexus remaining: "+health);
        }
    }

    public void TakeDamage(int Damage)
    {
        soundEffects.SetCurrentSound(SoundEffect.SOUND_EFFECT.HIT);
        soundEffects.triggerSound();


        health-=Damage;

        damageColorOn = true;

        //Update Healthbar
        UIController.UpdateNexusHealth(health);

        //Call UI Damage Effect Here
        //UIController.

        // Die if health goes below 0
        if (health <= 0)
        {
            
            if(!GameObject.Find("DevTools").GetComponent<DevTools>().getDevmode()){
                Death();
            }
        }


    }

    public void Death()
    {
        Debug.Log("nexus Die");
        
        soundEffectDeaths.MakeDeathSound(SoundEffectDeathsounds.SOUND_EFFECT_DEATHS.NEXUS, .3f);
        SceneManager.LoadScene(2);

    }
}
