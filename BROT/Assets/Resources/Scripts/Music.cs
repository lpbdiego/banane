using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;
using UnityEngine.Rendering;

public class Music : MasterVolume
{
    public int state = 0;

    public AudioClip soundtrack;
    public bool loop = true;


    new void Update() {
        base.Update();
        if(volume != musicVolume) {
            volume = musicVolume;
            changeVolume(volume);
        }

        if(loop && !source.isPlaying) {
            Play();
        }
    }
    


    void SetSoundtrack(AudioClip clip) {
        this.soundtrack = clip;
    }

    new void Start() {
        base.Start();
        changeVolume(volumeGain);
        Play();
    }


    public void changeVolume(float vol) {
        source.volume = vol;
    }

    public void triggerSound() {
        source.Play();
    }

    public void Play() {
        source.PlayOneShot(soundtrack);
    }

    public void setLoop(bool boo) {
        source.loop = boo;
    }

    public void Stop() {
        source.Stop();
    }
    public void Pause() {
        source.Pause();
    }


    public bool GetPlaying(){return source.isPlaying;}



}
