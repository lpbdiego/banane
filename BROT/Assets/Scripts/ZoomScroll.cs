using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ZoomScroll : MonoBehaviour
{
    public Camera ZoomCamera;

    // Start is called before the first frame update
    void Start()
    {
        ZoomCamera = Camera.main;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (ZoomCamera.orthographicSize - Input.GetAxis("Mouse ScrollWheel") * 5 >= 0.5f && ZoomCamera.orthographicSize - Input.GetAxis("Mouse ScrollWheel") * 5 <= 5f){
            ZoomCamera.orthographicSize -= Input.GetAxis("Mouse ScrollWheel") * 5;
            
        }
    }
}
