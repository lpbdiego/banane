using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
*@authors: Kai
*/
public class StructureInfos : MonoBehaviour
{
    /**
    * Structure spritesheet is the name of the Spritesheet placed inside the resources folder.
    */
    public string structureSpritesheet;
    public float maxHp;
    [SerializeField] private float hp;
    private new string name;
    private GameObject leftOver; 

    private GameObject prefab;
    public int cost = 0;
    public Sprite destroyedStructure;
    public Sprite structureSprite;



    public GameObject prefabSecond;

    private bool leftALeftover = false;

    public bool hasDestructAnimation = false;
    public bool hasDestructAnim() {return hasDestructAnimation;}
    public void setLeftALeftover(bool boo) { leftALeftover = boo; }
    public bool getLeftALeftover() {return leftALeftover;}



    public int getCost() {return cost;}


    public void setMaxHP(float val)
    {
        maxHp = val;
    }
    public void setHp(float val)
    {
        hp = val;
    }

    public float getMaxHp()
    {
        return maxHp;
    }
    public float getHp()
    {
        return hp;
    }

    public string getStructureSpritesheet()
    {
        return structureSpritesheet;
    }

    
    public Sprite getStructureSpritePreview()
    {
        return structureSprite;
    }
    public Sprite getStructureSpriteDestroyed()
    {
        return destroyedStructure;
    }

    public void setPrefab(GameObject obj)
    {
        prefab = obj;
    }
    public GameObject getPrefab()
    {
        return prefab;
    }

    public string getName()
    {
        return name;
    }

    public void setName(string name) { this.name = name; }
    public GameObject getSecondPrefab() {return prefabSecond;}

    /**The second prefab is for connections or something else, fill with null, if it isnt needed
    */
    public void setAll(string name, GameObject prefab, string spritesheet, float maxHp, GameObject prefabSecond, int cost) {
        this.name = name;
        this.prefab = prefab;
        structureSpritesheet = spritesheet;
        this.maxHp = maxHp;
        this.prefabSecond = prefabSecond;
        this.cost = cost;
    }
}
