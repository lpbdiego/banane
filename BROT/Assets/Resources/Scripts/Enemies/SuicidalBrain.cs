using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.VisualScripting;
using UnityEngine;
/*
*@authors: Kai
*/
public class SuicidalBrain : Enemy
{
    // Start is called before the first frame update
    Animator anim;
    public int explodingHitPoints = 1;
    public float explodeRadius;
    SoundEffect soundEffect;
    SoundEffectDeathsounds soundEffectDeath;

    new void Start()
    {
        anim = this.GetComponentInChildren<Animator>();
        base.Start();
        
        
        //audio = gameObject.transform.Find("Audio").gameObject;
        soundEffect = GetComponentInChildren<SoundEffect>();
        //soundEffect.changeVolume(.2f);

        
        soundEffectDeath = GetComponentInChildren<SoundEffectDeathsounds>();
        //soundEffectDeath.changeVolume(.2f);

    }
    new void Update()
    {
        if(anim.GetBool("explode"))
            target = null;
        else
            base.Update();

        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Destroy"))
        {
                Die();
        }
    }

    new void Die()
    {
        base.Die();
    }



    public override void Attack()
    {
       soundEffectDeath.MakeDeathSound(SoundEffectDeathsounds.SOUND_EFFECT_DEATHS.ENEMY_SUICIDE, .5f);

        GameObject tmp = Instantiate(attackPrefab, transform.position, Quaternion.Inverse(transform.rotation));
        tmp.GetComponent<AttackPoints>().setAttackPoints(explodingHitPoints);
        //Debug.Log("ITS A ME: "+explodingHitPoints);
        tmp.GetComponent<ResizeCollision>().setRadius(explodeRadius);
        
        
        target = null;
        anim.SetBool("explode", true);

    }

}
