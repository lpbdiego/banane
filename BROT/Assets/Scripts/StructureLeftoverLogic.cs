using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/*
*@authors: Kai
*/
public class StructureLeftoverLogic : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
    }

    void Update()
    {

    }


    public void repair()
    {

        StructureInfos infos = this.GetComponent<StructureInfos>();

        GameObject tmp = (GameObject)Instantiate(infos.getPrefab(), this.transform.position, this.transform.rotation);
        tmp.GetComponent<StructureInfos>().setAll(infos.getName(), infos.getPrefab(), infos.getStructureSpritesheet(), infos.getMaxHp(), infos.getSecondPrefab(), infos.getCost());
        tmp.GetComponent<StructureInfos>().setHp(infos.getMaxHp());



        tmp.AddComponent<EventClickPlaceables>();
        tmp.GetComponent<EventClickPlaceables>().setController(GameObject.Find("PlacableController").gameObject);
        tmp.name = infos.getName();

        tmp.AddComponent<StructureDestroyBeahaviour>();
        tmp.GetComponent<StructureDestroyBeahaviour>().set(tmp.GetComponent<StructureInfos>(), tmp);


        PlacingController.structs.Remove(this.gameObject);
        PlacingController.structs.Add(tmp);

        Destroy(this.gameObject);

    }
}


