using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;
using UnityEngine.Rendering;

public class SoundEffect : MasterVolume
{
    public int state = 0;


    [Header("Normal, Die, Attack, Hit ,stun, special, pickup")]
    public AudioClip[] clips = new AudioClip[7];
    private AudioClip clip;


    public enum SOUND_EFFECT
    {
        NORMAL,
        DIE,
        ATTACK,
        HIT,
        STUN,
        SPECIAL,
        PICKUP
    }

    
    public SOUND_EFFECT selected_effect = SOUND_EFFECT.NORMAL;

        new void Update() {
        base.Update();        
        if(volume != soundEffectsVolume) {
            volume = soundEffectsVolume;
            changeVolume(volume);
        }
    }
    AudioClip getCurrentClip() {
        switch(selected_effect) {
            case SOUND_EFFECT.NORMAL: return clips[0];
            case SOUND_EFFECT.DIE: return clips[1];
            case SOUND_EFFECT.ATTACK: return clips[2];
            case SOUND_EFFECT.HIT: return clips[3];
            case SOUND_EFFECT.STUN: return clips[4];
            case SOUND_EFFECT.SPECIAL: return clips[5];
            case SOUND_EFFECT.PICKUP: return clips[6];
            default: return clips[0];
        }
    }

    public AudioClip SetCurrentSound(SOUND_EFFECT snd) {
        selected_effect = snd;
        clip = getCurrentClip();
        return clip;
    }



    void setClip(AudioClip clip) {
        this.clip = clip;
    }

    new void Start() {
        base.Start();
        changeVolume(volumeGain);
        SetCurrentSound(SOUND_EFFECT.NORMAL);
    }


    public void changeVolume(float vol) {
        source.volume = vol;
    }

    public void triggerSound() {
        source.PlayOneShot(getCurrentClip());
    }


    public void setLoop(bool boo) {
        source.loop = boo;
    }

    public void stop() {
        source.Stop();
    }


    public bool GetPlaying(){return source.isPlaying;}



}
