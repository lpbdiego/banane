using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterSeconds : MonoBehaviour //by Diego
{
    // Public variable to set the delay time in seconds in the Unity Inspector
    public float delay = 1f;
    void Start()
    {
        // Call the DestroyGameObject method after the specified delay
        Invoke("DestroyGameObject", delay);
    }

    void DestroyGameObject()
    {
        // Destroy the game object
        Destroy(gameObject);
    }
}
