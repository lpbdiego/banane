using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
*@authors: Kai
*/
public class StructureOnDestroy : MonoBehaviour
{

    /**Just for generall destruction, if anything like tesla has other things to do while destroying, implement this method seperately with changes in the Structures script*/
    void OnDestroy()
    {
        StructureInfos infos = this.GetComponent<StructureInfos>();
        if (infos.getLeftALeftover())
        {
            Sprite[] sprites = Resources.LoadAll<Sprite>(infos.getStructureSpritesheet());
            Sprite spr = infos.getStructureSpriteDestroyed();
            GameObject tmp = (GameObject)Instantiate(Resources.Load("StructureLeftover", typeof(GameObject)), this.transform.position, this.transform.rotation);
            tmp.AddComponent<StructureInfos>();
            tmp.GetComponent<StructureInfos>().setAll(infos.getName(), infos.getPrefab(), infos.getStructureSpritesheet(), infos.getMaxHp(), null, infos.getCost());
            tmp.GetComponent<SpriteRenderer>().sprite = spr;
            tmp.name = "StructureLeftover";
            
            PlacingController.structs.Remove(this.gameObject);
            PlacingController.structs.Add(tmp);
            infos.setLeftALeftover(false);
        }
    }

}
