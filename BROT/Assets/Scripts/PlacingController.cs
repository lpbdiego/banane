using System;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
/*
*@authors: Kai
*/
public class PlacingController : MonoBehaviour
{

    [SerializeField] private GameObject[] structuresToBuild;
    private Vector3 worldPosition;
    private Vector3 screenPosition;
    private GameObject currentStructure;
    private bool placeMode = false;
    private int index = 0;
    private bool isSelected = false;
    public static int turretsPlaced = 0;
    public static int fencesPlaced = 0;
    public static int flamethrowersPlaced = 0;

    //cost multiplier
    public static int costMulti = 2;


    Player player;
    UIController UIController;

    bool toExpensive = false;

    private GameObject mouseSprite;
    SoundEffect soundEffects;

    public static bool repairAllowed  =false;


    public static List<GameObject> structs = new List<GameObject>();

    public int getIndex()
    {
        return index;
    }



    void Start()
    {

        UIController = GameObject.FindGameObjectWithTag("UIController").GetComponent<UIController>();
        soundEffects = GetComponentInChildren<SoundEffect>();

        player = GameObject.Find("Player").GetComponent<Player>();
        player.scrap = 0;

        // if (structuresToBuild.Length >= 1)
        // currentStructure = structuresToBuild[0];
    }

    void getMouseSprite()
    {
        if (!GameObject.Find("DevTools").GetComponent<DevTools>().getDevmode())
        {
            if (index == 0)
            {

                toExpensive = player.scrap + 1 <= currentStructure.GetComponent<StructureInfos>().getCost() + turretsPlaced * costMulti ? true : false;
            }
            if (index == 1)
            {
                toExpensive = player.scrap + 1 <= currentStructure.GetComponent<StructureInfos>().getCost() + fencesPlaced * costMulti ? true : false;
            }
            if (index == 2)
            {
                toExpensive = player.scrap + 1 <= currentStructure.GetComponent<StructureInfos>().getCost() + flamethrowersPlaced * costMulti ? true : false;
            }
        }

        if (mouseSprite != null)
            Destroy(mouseSprite);
        mouseSprite = Instantiate(Resources.Load<GameObject>("Prefabs/MouseSprite"), this.transform.position, this.transform.rotation);
        SpriteRenderer sr = mouseSprite.GetComponent<SpriteRenderer>();
        sr.sprite = currentStructure.GetComponentInChildren<StructureInfos>().getStructureSpritePreview();

        sr.color = toExpensive ? new Color(1f, .5f, .5f, 0.25f) : new Color(.5f, .5f, 1f, 0.25f);
    }

    void selectedSound()
    {
        soundEffects.SetCurrentSound(SoundEffect.SOUND_EFFECT.STUN);
        soundEffects.triggerSound();
    }
    void Update()
    {
        if (!PauseMenu.GameIsPaused)
        {
            if (Input.GetKeyDown("1") && structuresToBuild.Length >= 1)
            {
                selectedSound();
                placeMode = true;
                EventClickPlaceables.moveMode = false;
                if (placeMode)
                {
                    Debug.Log("Structure 1 selected!");
                    currentStructure = structuresToBuild[0];
                    index = 0;
                    isSelected = true;
                    getMouseSprite();
                    UIController.TurretButtonPressed(1);
                }
            }

            if (Input.GetKeyDown("2") && structuresToBuild.Length >= 2)
            {
                selectedSound();
                placeMode = true;
                EventClickPlaceables.moveMode = false;
                if (placeMode)
                {
                    Debug.Log("Structure 2 selected!");
                    currentStructure = structuresToBuild[1];
                    index = 1;
                    isSelected = true;
                    getMouseSprite();
                    UIController.TurretButtonPressed(2);
                    mouseSprite.GetComponent<MouseSprite>().setAdditionals("electricity");
                }
            }

            if (Input.GetKeyDown("3") && structuresToBuild.Length >= 3)
            {
                selectedSound();
                placeMode = true;
                EventClickPlaceables.moveMode = false;
                if (placeMode)
                {
                    Debug.Log("Structure 3 selected!");
                    currentStructure = structuresToBuild[2];
                    index = 2;
                    isSelected = true;
                    getMouseSprite();
                    UIController.TurretButtonPressed(3);

                }
            }
            if (Input.GetKeyDown(KeyCode.R) && !isSelected) //Repair
            {
                if (repairAllowed)
                {
                    UIController.hideRepairIcon();
                    repairAllowed = false;
                    selectedSound();
                    EventClickPlaceables.moveMode = false;

                    for (int i = structs.Count-1; i >= 0; i--)
                    {
                        Debug.Log(structs[i].name);
                        if (structs[i].name.Equals("StructureLeftover"))
                        {
                            structs[i].GetComponent<StructureLeftoverLogic>().repair();
                        }
                    }
                }
            }
            if (Input.GetKeyDown("5") && structuresToBuild.Length >= 5 && !isSelected)
            {
                selectedSound();
                placeMode = true;
                EventClickPlaceables.moveMode = false;
                if (placeMode)
                {
                    Debug.Log("Structure 5 selected!");
                    currentStructure = structuresToBuild[4];
                    index = 4;
                    isSelected = true;
                    getMouseSprite();
                    UIController.TurretButtonPressed(5);

                }
            }

            if (Input.GetKeyDown("6") && structuresToBuild.Length >= 6 && !isSelected)
            {
                selectedSound();
                placeMode = true;
                EventClickPlaceables.moveMode = false;
                if (placeMode)
                {
                    Debug.Log("Structure 6 selected!");
                    currentStructure = structuresToBuild[5];
                    index = 5;
                    isSelected = true;
                    getMouseSprite();
                    UIController.TurretButtonPressed(6);

                }
            }

            if (Input.GetKeyDown("7") && structuresToBuild.Length >= 7 && !isSelected)
            {
                selectedSound();
                placeMode = true;
                EventClickPlaceables.moveMode = false;
                if (placeMode)
                {
                    Debug.Log("Structure 7 selected!");
                    currentStructure = structuresToBuild[6];
                    index = 6;
                    isSelected = true;
                    getMouseSprite();
                    UIController.TurretButtonPressed(7);

                }
            }

            if (Input.GetKeyDown("8") && structuresToBuild.Length >= 8 && !isSelected)
            {
                selectedSound();
                placeMode = true;
                EventClickPlaceables.moveMode = false;
                if (placeMode)
                {
                    Debug.Log("Structure 8 selected!");
                    currentStructure = structuresToBuild[7];
                    index = 7;
                    isSelected = true;
                    getMouseSprite();
                    UIController.TurretButtonPressed(8);

                }
            }
            if (Input.GetKeyDown("9") && structuresToBuild.Length >= 9 && !isSelected)
            {
                selectedSound();
                placeMode = true;
                EventClickPlaceables.moveMode = false;
                if (placeMode)
                {
                    Debug.Log("Structure 9 selected!");
                    currentStructure = structuresToBuild[8];
                    index = 8;
                    isSelected = true;
                    getMouseSprite();
                    UIController.TurretButtonPressed(9);

                }
            }
            if (Input.GetKeyDown("0") && structuresToBuild.Length >= 10 && !isSelected)
            {
                selectedSound();
                placeMode = true;
                EventClickPlaceables.moveMode = false;
                if (placeMode)
                {
                    Debug.Log("Structure 10 selected!");
                    currentStructure = structuresToBuild[9];
                    index = 9;
                    isSelected = true;
                    getMouseSprite();
                    UIController.TurretButtonPressed(10);

                }
            }

            if (Input.GetKeyDown(KeyCode.M))
            {
              //  EventClickPlaceables.moveMode = !EventClickPlaceables.moveMode;
              //  placeMode = false;


                //if (EventClickPlaceables.moveMode)
                //    Debug.Log("Move mode is enabled!, disabled Placemode if it was enabled");
                ////else
            //        Debug.Log("Move Mode is disabled");
            }
            /*
            if (Input.GetKeyDown(KeyCode.Q))
            {
                placeMode = !placeMode;
                if (EventClickPlaceables.moveMode)
                    EventClickPlaceables.moveMode = false;
                if (placeMode)
                    Debug.Log("Place mode is enabled!, disabled move mode  if it was enabled");
                else
                    Debug.Log("Place Mode is disabled");
            }*/

            if (Input.GetMouseButtonUp(1))
            {
                if (placeMode && isSelected && currentStructure != null)
                {

                    //change this to >= 3 later
                    if (!toExpensive)//if (player.scrap <= 300000)
                    {

                        if (index == 0)
                        {
                            player.scrap = player.scrap - currentStructure.GetComponent<StructureInfos>().getCost() - turretsPlaced * costMulti;
                            turretsPlaced += 1;
                            UIController.setCostText(1, currentStructure.GetComponent<StructureInfos>().getCost() + turretsPlaced * costMulti);

                        }
                        if (index == 1)
                        {
                            player.scrap = player.scrap - currentStructure.GetComponent<StructureInfos>().getCost() - fencesPlaced * costMulti;
                            fencesPlaced += 1;
                            UIController.setCostText(2, currentStructure.GetComponent<StructureInfos>().getCost() + fencesPlaced * costMulti);

                        }
                        if (index == 2)
                        {
                            player.scrap = player.scrap - currentStructure.GetComponent<StructureInfos>().getCost() - flamethrowersPlaced * costMulti;
                            flamethrowersPlaced += 1;
                            UIController.setCostText(3, currentStructure.GetComponent<StructureInfos>().getCost() + flamethrowersPlaced * costMulti);


                        }

                        UIController.UpdateScrapCounter(player.scrap);

                        placeMe(currentStructure);
                        UIController.ResetTurretButtons();


                        soundEffects.SetCurrentSound(SoundEffect.SOUND_EFFECT.NORMAL);
                        soundEffects.triggerSound();

                    }
                    else
                    {
                        toExpensive = false;
                        isSelected = false;
                        Destroy(mouseSprite);
                        Debug.Log("Insufficient amount of scrap");
                        UIController.ResetTurretButtons();

                    }
                }
            }

            if (Input.GetMouseButtonUp(0))
            {
                toExpensive = false;
                isSelected = false;
                Destroy(mouseSprite);
                UIController.ResetTurretButtons();




            }
        }
    }
    void placeMe(GameObject curStructure)
    {
        transform.position = new Vector3(mouseSprite.transform.position.x, mouseSprite.transform.position.y, 0.0f);
        Destroy(mouseSprite);

        GameObject tmp = Instantiate(curStructure, this.transform.position, this.transform.rotation);
        tmp.AddComponent<EventClickPlaceables>();
        tmp.GetComponent<EventClickPlaceables>().setController(this.gameObject);
        tmp.AddComponent<StructureDestroyBeahaviour>();
        tmp.GetComponent<StructureDestroyBeahaviour>().set(tmp.GetComponent<StructureInfos>(), tmp);
        tmp.GetComponent<StructureInfos>().setPrefab(curStructure);
        tmp.GetComponent<StructureInfos>().setName(curStructure.name);
        tmp.name = curStructure.name;
        structs.Add(tmp);
        isSelected = false;
    }

    /**
    *   To access your reposition code, extend the switch case with your Structures Name and the Script, it should work then
    */
    public void reposition(GameObject obj)
    {
        switch (obj.name)
        {
            case "Tesla_Tower": obj.GetComponent<TeslaTower>().reposition(); break;
            default:; break;
        }
    }
}
