using System;
using UnityEngine;
/*
*@authors: Kai
*/
/** Timer Script, to implement Timers on Objects.
*   Now it need to be inherited from instead of MonoBehaviour, the timer now supports full seconds and tenth of seconds(somehow it ticks twice for each tenth it seems)
*   Any variable/function starts with t_/T_ to prevent overrides, the Update and Start of the ineheritant class need to be stated as "new void" and need to use base.Update() /base.Start() to work properly
*   While overiding "public override void T_TICK_BEHAVIOUR() {Debug.Log("Definetly modiefied!");}"  you could define what should happen every tick of the Timer.
*
*   An enum is used to keep track of the state, with the functions the Timer is controllable within other scripts.
*   with T_GetState() you can expand the behaviour of others more specific than just using getFinish()
*   if debug is set to true, the remaining seconds are displayed, msg could be altered to give further information
*/
public class Timer : MonoBehaviour
{
    public enum TIMER_STATE
    {
        PLAY,
        PAUSE,
        STOP,
        IDLE
    }
    private float t_currentTime;
    private float t_previous = 0;
    private bool t_finish = false;
    private TIMER_STATE t_state = TIMER_STATE.IDLE;
    [Header("Timer")]
    [Space(10)]    public float t_startTime = 5.0f;
    public bool t_debug = false;
    public String t_msg = "";
    public bool t_directStart = false;
    public bool t_fullSecond = true;
    public bool t_isWaveTimer = false;

    
    private int previousTenthSecond = 11;
    private int currentTenthSecond = 0;


    UIController t_UIController;



    public void Start()
    {
        if (t_isWaveTimer)
            t_UIController = GameObject.FindGameObjectWithTag("UIController").GetComponent<UIController>();

        this.t_currentTime = this.t_startTime;
        this.t_previous = t_fullSecond ? Mathf.RoundToInt(this.t_currentTime) : this.t_currentTime;
        if (t_directStart)
            this.T_Play();

    }


    public void T_SetTimerDuration(float startTime)
    {
        this.t_startTime = startTime;
        this.t_previous = t_fullSecond ? Mathf.RoundToInt(this.t_currentTime) : this.t_currentTime;
    }

    public void T_AddStartTime(float startTime)
    {
        this.t_startTime = startTime;
        this.t_previous = t_fullSecond ? Mathf.RoundToInt(this.t_currentTime) : this.t_currentTime;
    }

    public void T_Play()
    {
        this.t_state = TIMER_STATE.PLAY;
    }

    public void T_Stop()
    {
        this.t_state = TIMER_STATE.STOP;
    }

    public void T_Pause()
    {
        this.t_state = TIMER_STATE.PAUSE;
    }

    public void T_Resume()
    {
        this.t_state = TIMER_STATE.PLAY;
    }

    public void T_Reset()
    {
        this.t_currentTime = this.t_startTime;
        this.t_previous = t_fullSecond ? Mathf.RoundToInt(this.t_currentTime) : this.t_currentTime;
        this.t_finish = false;
        this.t_state = TIMER_STATE.IDLE;
    }

    // Update is called once per frame
    public void Update()
    {
        if (this.t_state == TIMER_STATE.PLAY)
        {
            if (t_currentTime <= 0.0f)
            {
                this.t_finish = true;
                this.T_Stop();
            }
            else
            {
                this.t_currentTime -= 1.0f * Time.deltaTime;
                // this.t_currentTime = Mathf.Round(this.t_currentTime * 10.0f) * 0.1f;
                OwnTickingBehaviour();
            }
        }
        else
        {
            if (this.t_state == TIMER_STATE.PAUSE)
            {

            }
            if (this.t_state == TIMER_STATE.STOP)
            {

            }
        }
    }
    private void OwnTickingBehaviour()
    {
        if (t_previous > Mathf.RoundToInt(t_currentTime) && t_fullSecond)
        {

            t_previous = Mathf.RoundToInt(t_currentTime);
            T_TICK_BEHAVIOUR();
            if (t_debug)
                T_Message(true);
        }
        else if (!t_fullSecond)
        {
            t_previous = t_currentTime;
            //Debug.Log("CurrentTime: "+this.t_currentTime);
            currentTenthSecond = Mathf.RoundToInt(t_currentTime % 1 * 10.0f);
            //Debug.Log("CurrentTenthSec : " + currentTenthSecond);

            if (previousTenthSecond > currentTenthSecond)
            {
                previousTenthSecond = Mathf.RoundToInt(currentTenthSecond);
                if(previousTenthSecond <= 0)
                    previousTenthSecond = 11;
                //t_previous = Mathf.RoundToInt(t_currentTime);
                T_TICK_BEHAVIOUR();
                if (t_debug)
                    T_Message(false);
            }
        }
    }
    void T_Message(bool fullSecond)
    {
        if (fullSecond)
        {
            Debug.Log(Mathf.RoundToInt(t_currentTime) + 1 + t_msg);
            if (t_isWaveTimer)
                t_UIController.UpdateWaveCountdown(Mathf.RoundToInt(t_currentTime));
        }
        else
        {
            Debug.Log(t_currentTime + 1 + t_msg);
        }
    }

    public bool T_GetTimerFinish()
    {
        return t_finish;
    }

    public TIMER_STATE T_GetTimerState()
    {
        return t_state;
    }

    public void T_SetFullSecond(bool boo) {t_fullSecond = boo;}
    public bool T_GetFullSecond(){return t_fullSecond;}

    public void T_SetWaveTimer(bool boo) { this.t_isWaveTimer = boo; }

    public virtual void T_TICK_BEHAVIOUR() { }
}
