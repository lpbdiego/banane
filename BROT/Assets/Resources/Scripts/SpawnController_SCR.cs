using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
/*
*@authors: Kai
*/
/**
 * Currently the spawn controller is designed to take the game objects of the enemies and the spawnpoints(spawnpoints need to be placed in the scene at first and added to the script),
 * this script should handle them without editing this script.
 * 
 * The rate and spawner by Modulo parameters are used to determine in which wave the addRate is been modified,
 *      and when the next spawner sets to be active
 * 



 * This is now a working demo which starts automatically and spawns the enemies with a randomness lifespan, 
 * after every enemy per wave is dead, a cooldown takes place, after this a new wave begins.
 */
public class SpawnController_SCR : Timer
{


    private bool newWave = false;
    private List<GameObject> enemies = new List<GameObject>();

    [Header("-------------")]
    [Space(5)]
    [Header("Spawner")]
    [Space(5)]
    public GameObject[] enemyTypes;
    public GameObject[] spawnPoint;
    public float waveCooldownValue = 10.0f;
    public int wave = 0;
    public static int currentWave = 0; //EASY BNECAUSE LAZY LMAO
    public int spawnRate = 3;
    public int rateModifierByWaveModulo = 10;
    public int activeSpawnerModifierByWaveModulo = 5;
    public int activeSpawner = 1;
    public int addRate = 3;
    public int addRateModifier = 3;
    public bool triggerWave = false;

    UIController UIController;
    // Start is called before the first frame update
    Highscore highscore;

    new void Start()
    {
        base.Start();
        UIController = GameObject.FindGameObjectWithTag("UIController").GetComponent<UIController>();

        highscore = FindObjectOfType<Highscore>();


        T_SetTimerDuration(waveCooldownValue);
        WaveCooldown();
    }

    // Update is called once per frame
    new void Update()
    {
        base.Update();

        if (T_GetTimerFinish())
        {
            T_Reset();
            triggerWave = true;
        }
        ClearEnemyList();
        if (triggerWave)
        {
            triggerWave = false;
            if (enemies.Count == 0)
            {
                if (wave != 0)
                {
                    addRate = wave % rateModifierByWaveModulo == 0 ? addRate + addRateModifier : addRate;
                    spawnRate += addRate;
                }
                wave++;
                PlacingController.repairAllowed = true;
                UIController.showRepairIcon();
                UIController.UpdateWaveCounter(wave);

                highscore.UpdatePersonalBestWave(wave); // Call the method to update personal best wave


                newWave = true;
            }
        }

        if (newWave)
        {
            newWave = false;

            for (int i = 0; i < spawnPoint.Length; i++)
            {
                spawnPoint[i].gameObject.GetComponent<SpriteRenderer>().enabled = false;
            }

            for (int k = 0; k < spawnRate; k++)
            {
                Vector3 vec = new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), 0.0f);
                int randLength = enemyTypes.Length;
                int rand = randomize(randLength);
                Debug.Log("ITS a me: "+rand);
                rand = rand == 1 ? randomize(randLength) : rand;
                rand = rand == 1 ? randomize(randLength) : rand;
                rand = rand == 1 ? randomize(randLength) : rand;
                enemies.Add(Instantiate(enemyTypes[rand], this.spawnPoint[Random.Range(0, activeSpawner)].transform.position + vec, this.transform.rotation) as GameObject);
            }
        }

    }

    public int randomize(int val){
        return Random.Range(0, val);
    }

    void WaveCooldown()
    {
        T_Play();

        if (wave == 0)
        {
            spawnPoint[0].gameObject.GetComponent<SpriteRenderer>().enabled = true;

        }

        activeSpawner = (wave + 1) % activeSpawnerModifierByWaveModulo == 0 ? activeSpawner < spawnPoint.Length ? activeSpawner + 1 : activeSpawner : activeSpawner;

        for (int i = 0; i < activeSpawner; i++)
        {

            spawnPoint[i].gameObject.GetComponent<SpriteRenderer>().enabled = true;

        }


    }

    void ClearEnemyList()
    {
        if (enemies.Count != 0)
        {
            bool clear = true;
            for (int i = 0; i < enemies.Count; i++)
            {
                if (enemies[i] != null)
                {
                    clear = false;
                    break;
                }
            }
            if (clear)
            {
                clear = false;
                enemies.Clear();
                WaveCooldown();
            }
        }
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    // called second
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        wave = 0;
    }
}