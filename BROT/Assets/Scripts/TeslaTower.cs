using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.U2D;
/*
*@authors: Kai
*/
public class TeslaTower : MonoBehaviour
{

    StructureInfos infos;
    private GameObject electricity;
    public float fenceDistance;
    private static List<GameObject> fence_posts = new List<GameObject>();
    private static List<GameObject> fence_connections = new List<GameObject>();
    public static List<GameObject> getTesla()  {return fence_posts;}



        static SoundEffect soundEffects;

    void Start()
    {
        soundEffects = GetComponentInChildren<SoundEffect>();


        this.infos = this.GetComponent<StructureInfos>();
        this.infos.setHp(this.infos.getMaxHp());
        electricity = this.infos.getSecondPrefab();

        fence_posts.Add(this.gameObject);

        for (int i = 0; i < fence_posts.Count; i++)
        {
            if (fence_posts[i] != this.gameObject)
            {
                float distance = Vector3.Distance(this.transform.position, fence_posts[i].transform.position);
                //Debug.Log(distance);
                if (distance < fenceDistance)
                {
                    fence_connections.Add(Instantiate(electricity, this.transform.position, this.transform.rotation * fence_posts[i].transform.rotation) as GameObject);
                    fence_connections[fence_connections.Count - 1].GetComponent<Electricity>().setFencePosts(this.gameObject, fence_posts[i]);
                    fence_connections[fence_connections.Count - 1].GetComponent<Electricity>().SetPositions(this.transform, fence_posts[i].transform);
                }
            }
        }
    }

    void Update()
    {
        //if(!soundEffects.GetPlaying())
         //   soundEffects.triggerSound();

    }

    public void reposition()
    {
        for (int k = 0; k < 3; k++)
        {
            for (int i = 0; i < fence_connections.Count; i++)
            {

                GameObject A = fence_connections[i].GetComponent<Electricity>().getFencePosts(0);
                GameObject B = fence_connections[i].GetComponent<Electricity>().getFencePosts(1);
                float distance;
                if (A == this.gameObject || B == this.gameObject)
                {
                    if (A == this.gameObject)
                        distance = Vector3.Distance(this.transform.position, B.transform.position);
                    else
                        distance = Vector3.Distance(A.transform.position, this.transform.position);


                    if (distance < fenceDistance)
                    {
                        fence_connections[i].GetComponent<Electricity>().setPositionsWithself();
                    }
                    else
                    {
                        GameObject tmp = fence_connections[i];
                        fence_connections.Remove(tmp);
                        Destroy(tmp);
                    }

                }
            }
        }

        List<GameObject[]> fence_connections_Posts = new();
        for (int b = 0; b < fence_connections.Count; b++)
        {
            fence_connections_Posts.Add(new GameObject[] { fence_connections[b].GetComponent<Electricity>().getFencePosts(0), fence_connections[b].GetComponent<Electricity>().getFencePosts(1) });
        }

        for (int a = 0; a < fence_posts.Count; a++)
        {
            bool isConnected = false;
            for (int c = 0; c < fence_connections_Posts.Count; c++)
            {
                if ((this.gameObject == fence_connections_Posts[c][0] && fence_posts[a] == fence_connections_Posts[c][1]) || (this.gameObject == fence_connections_Posts[c][1] && fence_posts[a] == fence_connections_Posts[c][0]))
                {
                    isConnected = true;
                    break;
                }
            }

            if (!isConnected)
            {
                if (this.gameObject != fence_posts[a])
                {
                    float distance = Vector3.Distance(this.transform.position, fence_posts[a].transform.position);

                    if (distance < fenceDistance)
                    {
                        fence_connections.Add(Instantiate(electricity, this.transform.position, this.transform.rotation * fence_posts[a].transform.rotation) as GameObject);
                        fence_connections[fence_connections.Count - 1].GetComponent<Electricity>().setFencePosts(this.gameObject, fence_posts[a]);
                        fence_connections[fence_connections.Count - 1].GetComponent<Electricity>().SetPositions(this.transform, fence_posts[a].transform);
                    }

                }
            }
        }
    }


    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    // called second
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        fence_posts.Clear();
        fence_connections.Clear();
    }

    void OnDestroy()
    {
        if (infos.getLeftALeftover())
        {
            GameObject tmp = (GameObject)Instantiate(Resources.Load("StructureLeftover", typeof(GameObject)), this.transform.position, this.transform.rotation);
            tmp.AddComponent<StructureInfos>();
            tmp.GetComponent<StructureInfos>().setAll(infos.getName(), infos.getPrefab(), infos.getStructureSpritesheet(), infos.getMaxHp(), electricity, infos.getCost());
            tmp.name = "StructureLeftover";
            PlacingController.structs.Remove(gameObject);
            PlacingController.structs.Add(tmp);

            for (int i = 0; i < fence_connections.Count; i++)
            {
                GameObject A = fence_connections[i].GetComponent<Electricity>().getFencePosts(0);
                GameObject B = fence_connections[i].GetComponent<Electricity>().getFencePosts(1);
                if (A == this.gameObject || B == this.gameObject)
                {
                    Destroy(fence_connections[i]);
                    fence_connections.RemoveAt(i);
                    i--;//Because deleting results in less entries
                }
            }
            fence_posts.Remove(this.gameObject);
            infos.setLeftALeftover(false);
        }
    }

}
