using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIController : MonoBehaviour
{


    public TMP_Text scrapCounter;
    public TMP_Text healthCounter;
    public TMP_Text waveCountdown;
    public TMP_Text waveCounter;

    public TMP_Text C1;
    public TMP_Text C2;
    public TMP_Text C3;


    public SpriteRenderer damageOverlay;
    public SpriteRenderer sprintOverlay;
    Color damageOverlayColor;
    Color sprintOverlayColor;
    bool damageOverlayOn = false;
    float damageOverlayCounter = 0;
    bool sprintOverlayOn = false;
    float sprintOverlayCounter = 0;
    RectTransform nexusHealth;
    RectTransform EnergyBar;
    RectTransform Supercharge;
    public GameObject RepairIcon;


    Image T1;
    Image T2;
    Image T3;





    // Start is called before the first frame update
    void Start()
    {
        nexusHealth = GameObject.FindGameObjectWithTag("NexusHealthBar").GetComponent<RectTransform>();
        EnergyBar = GameObject.FindGameObjectWithTag("Energy").GetComponent<RectTransform>();
        damageOverlay = GameObject.FindGameObjectWithTag("DamageOverlay").GetComponent<SpriteRenderer>();
        sprintOverlay = GameObject.FindGameObjectWithTag("SprintOverlay").GetComponent<SpriteRenderer>();
        Supercharge = GameObject.FindGameObjectWithTag("Supercharge").GetComponent<RectTransform>();
        damageOverlayColor = damageOverlay.color;
        sprintOverlayColor = sprintOverlay.color;


        T1 = GameObject.FindGameObjectWithTag("T1").GetComponent<Image>();
        T2 = GameObject.FindGameObjectWithTag("T2").GetComponent<Image>();
        T3 = GameObject.FindGameObjectWithTag("T3").GetComponent<Image>();

        C1 = GameObject.FindGameObjectWithTag("C1").GetComponent<TMP_Text>();
        C2 = GameObject.FindGameObjectWithTag("C2").GetComponent<TMP_Text>();
        C3 = GameObject.FindGameObjectWithTag("C3").GetComponent<TMP_Text>();

    }

    // Update is called once per frame
    void Update()
    {
        if (damageOverlayOn == true){

            damageOverlayCounter += Time.deltaTime * 200;

            if(damageOverlayCounter >= 0) {

                Color newColor = damageOverlayColor;
                newColor.a = damageOverlayCounter * 0.0005f;
                damageOverlay.color = newColor;
            }

            if(damageOverlayCounter >= 50) {

                Color newColor = damageOverlayColor;
                newColor.a = 0.025f - (damageOverlayCounter  - 50)*  0.0005f;
                damageOverlay.color = newColor;
            }

            if(damageOverlayCounter >= 100) {
                damageOverlayOn = false;
                damageOverlayCounter = 0;
            }
        }


        if (sprintOverlayOn == true){

            sprintOverlayCounter += Time.deltaTime * 400;

            if(sprintOverlayCounter >= 0) {

                Color newColor = sprintOverlayColor;
                newColor.a = sprintOverlayCounter * 0.0002f;
                sprintOverlay.color = newColor;
            }

            if(sprintOverlayCounter >= 100) {
                sprintOverlayCounter = 100;
                sprintOverlayOn = false;
            }

        } else if (sprintOverlayOn == false && sprintOverlayCounter > 0) {

            Color newColor = sprintOverlayColor;
            newColor.a = sprintOverlayCounter * 0.0002f;
            sprintOverlay.color = newColor;
            sprintOverlayCounter -= Time.deltaTime * 400;

            if (sprintOverlayCounter <= 0) {
                sprintOverlayCounter = 0;
            }
        }

        
    }



    //SCRAP COUNTER
    public void UpdateScrapCounter(float x)
    {
        scrapCounter.text = x.ToString() + " Scrap";
    }


    //HEALTH COUNTER <3
    public void UpdateHealthCounter(float x)
    {

    if (x % 10 == 0) {

        healthCounter.text = (x * 0.1f).ToString().Replace(",", ".") + ".0%";
    }
    else {

        healthCounter.text = (x * 0.1f).ToString().Replace(",", ".") + "%";

    }

    if (x == 1000) {
        healthCounter.text = "100%";
    }
    }


    //WAVE COUNTDOWN
    public void UpdateWaveCountdown(int x)
    {
        if(x == 0) {
            waveCountdown.text = "";

        }
        
        else {
            waveCountdown.text = "Next Wave in " + x.ToString();
        }
    }

    //WAVE COUNTER
    public void UpdateWaveCounter(int x)
    {
        waveCounter.text = "Wave " + x.ToString();

    }


    //NEXUS HEALTHBAR
    public void UpdateNexusHealth(int x)
    {
        nexusHealth.offsetMax = new Vector2(x / 10 - 100, nexusHealth.offsetMax.y);

    }

        //ENERGY BAR
    public void UpdateEnergyBar(float x)
    {
        EnergyBar.offsetMax = new Vector2(x - 100, EnergyBar.offsetMax.y);

    }

    public void ShowSprintOverlay()
    {
           sprintOverlayOn = true;
    }


        //SUPERCHARGE BAR
    public void UpdateSupercharge(float x)
    {
        if (x == 0){
            Supercharge.offsetMax = new Vector2(0 - 100, Supercharge.offsetMax.y);
        }
        else if (x >= 1.5f) {
            Supercharge.offsetMax = new Vector2(x * 10 - 115, Supercharge.offsetMax.y);
        }
    }

    //DAMAGE OVERLAY
    public void DamageOverlay()
    {
                damageOverlayOn = true;
    }


    //TURRET BUTTON PRESSED

    public void TurretButtonPressed(int x)
    {
        ResetTurretButtons();

        if (x == 1){
            T1.color = new Color(0.2f, 0.9f, 1f, 1f);
        }
        if (x == 2){
            T2.color = new Color(0.2f, 0.9f, 1f, 1f);
        }
        if (x == 3){
            T3.color = new Color(0.2f, 0.9f, 1f, 1f);
        }


    }

    public void ResetTurretButtons(){
            T1.color = new Color(0f, 0f, 0f, 1f);
            T2.color = new Color(0f, 0f, 0f, 1f);
            T3.color = new Color(0f, 0f, 0f, 1f);
    }


    public void setCostText(int x, int y)
    {
        if (x == 1){
            C1.text = y.ToString();
        }
        if (x == 2){
            C2.text = y.ToString();
        }
        if (x == 3){
            C3.text = y.ToString();
        }

    }

    public void showRepairIcon()
    {
       RepairIcon.SetActive(false);
    }

    public void hideRepairIcon()
    {
        RepairIcon.SetActive(true);
    }

/*
        StartCoroutine(TurretButton(x));

    public IEnumerator TurretButton(int x)
    {
        if (x == 1){
            T1.color = new Color(0.3f, 0.4f, 0.6f, 0.3f);
            yield return new WaitForSeconds(0.1f);
            T1.color = new Color(1f, 1f, 1f, 1f);
        }


    }
*/



}


