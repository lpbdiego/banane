using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
*@authors: Kai
*/
public class StructureDestroyBeahaviour : MonoBehaviour
{

    private Sprite[] sprites;
    private Sprite sprite;

    private SpriteRenderer sr;
    private StructureInfos infos;
    private float destroyModifier;
    private int spriteIndex = 0;
    private GameObject owner;
    // Start is called before the first frame update
    public void set(StructureInfos infos, GameObject owner)
    {
        this.owner = owner;
        this.infos = infos;
        if (infos.hasDestructAnim())
        {
            this.sr = this.GetComponentInChildren<SpriteRenderer>();

            this.sprites = Resources.LoadAll<Sprite>(infos.getStructureSpritesheet());
            this.sprite = sprites[0];
            this.sr.sprite = sprite;
            this.destroyModifier = this.infos.maxHp / this.sprites.Length;
        }
        //Debug.Log(this.destroyModifier);
    }
 
 /**/
    private void OnTriggerEnter2D(Collider2D collision)
    {
        this.infos = this.GetComponent<StructureInfos>();
        //Debug.Log(this.infos.getHp());
        if ( collision.gameObject.CompareTag("Boom") || collision.gameObject.layer == 10 || collision.gameObject.layer == 16)
        {
            //Destroy the bullet as well
            //Restart scene
            if( collision.gameObject.CompareTag("Boom"))
                this.infos.setHp(this.infos.getHp() - this.infos.getHp());
            else
                this.infos.setHp(this.infos.getHp() - 1);

            if (this.infos.getHp() % this.destroyModifier == 0 && infos.hasDestructAnim())
            {
                sprite = sprites[spriteIndex++];
                sr.sprite = sprite;
            }

            if (this.infos.getHp() <= 0)
            {
                this.infos.setLeftALeftover(true);
                Destroy(owner);
            }

        }
    }/**/
}
