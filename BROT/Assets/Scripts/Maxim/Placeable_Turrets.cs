using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Placeable_Turrets : MonoBehaviour
{
    [Header("*Variables to adjust*")]
    public GameObject ActiveBullet;
    public int bulletForce;
    public float distanceToStartShooting = 5f;
    public float shootingCooldown = 2f;
    SoundEffect soundEffects;


    [Header("*Information*")]
    public List<GameObject> enemiesInRange = new List<GameObject>();
    private GameObject currentTarget;

    public Transform aimTransform;
    public Transform aimGunEndPointTransform;

    private float lastShotTime;

    void Start() {
        soundEffects = GetComponentInChildren<SoundEffect>();
    }

    void Update()
    {
        UpdateTarget();

        if (currentTarget != null)
        {
            AimTowards(currentTarget.transform.position);

            if (Time.time - lastShotTime >= shootingCooldown)
            {
                Shoot(currentTarget.transform.position);
            }
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        

        if (collider.gameObject.CompareTag("Enemy") && collider.gameObject.name != "BulletEnemy(Clone)" )
        {
            enemiesInRange.Add(collider.gameObject);
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("Enemy")) 
        {
            enemiesInRange.Remove(collider.gameObject);
            if (currentTarget == collider.gameObject)
            {
                currentTarget = null;
            }
        }
    }

    

    void UpdateTarget()
    {
        if (currentTarget == null || !enemiesInRange.Contains(currentTarget))
        {
            float closestDistance = distanceToStartShooting;
            GameObject closestEnemy = null;

            foreach (GameObject enemy in enemiesInRange)
            {
                float distance = Vector2.Distance(transform.position, enemy.transform.position);
                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    closestEnemy = enemy;
                }
            }

            currentTarget = closestEnemy;
        }
    }

    void AimTowards(Vector3 targetPosition)
    {
        Vector3 aimDirection = (targetPosition - transform.position).normalized;
        float angle = Mathf.Atan2(aimDirection.y, aimDirection.x) * Mathf.Rad2Deg;
        aimTransform.eulerAngles = new Vector3(0, 0, angle + 90); // Adjust this if your turret's orientation needs a different offset
    }

    void Shoot(Vector3 targetPosition)
    {
        soundEffects.triggerSound();

        Vector3 shootDirection = (targetPosition - aimGunEndPointTransform.position).normalized;
        GameObject bullet = Instantiate(ActiveBullet, aimGunEndPointTransform.position, Quaternion.identity);
        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        rb.AddForce(shootDirection * bulletForce, ForceMode2D.Impulse);
        lastShotTime = Time.time;
    }
}
