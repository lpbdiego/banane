
using System;
using UnityEngine;

public class MasterVolume : MonoBehaviour
{
    public static float masterVolume = 1.0f;
    [HideInInspector] public float volumeGain = 1.0f;

    public static float musicVolume = 1.0f;
    [SerializeField] public float musicGain = 0.0f;
    public static float soundEffectsVolume = 1.0f;
    [SerializeField] public float soundEffectGain = soundEffectsVolume;

    public static Boolean muted = false;
    [SerializeField] public Boolean masterMute = false;

    public bool isMaster = false;
    private bool volumeChanged = false;

    public float volume = 1.0f;
    public AudioSource source;


    public void Start()
    {

        int deaf = 0;
        masterMute = false;

        musicVolume = musicGain;
        musicGain = musicVolume;
        soundEffectsVolume = soundEffectGain;
        soundEffectGain = soundEffectsVolume;
        if (isMaster)
            masterVolume = volumeGain;
        else
            volumeGain = masterVolume;
    }
    public void Update()
    {
        if (isMaster)
        {
         
        }


        if (isMaster && masterMute)
        {
            muted = true;

            PlayerPrefs.SetInt("Mute", 1);
            PlayerPrefs.Save();
        }
        if (isMaster && !masterMute)
        {
            muted = false;
            PlayerPrefs.SetInt("Mute", 0);
            PlayerPrefs.Save();
        }

        if (isMaster && musicVolume != musicGain)
        {
            musicVolume = musicGain;

            PlayerPrefs.SetFloat("MusicVolume", musicVolume);
            PlayerPrefs.Save();
        }


        if (isMaster && soundEffectsVolume != soundEffectGain)
        {
            soundEffectsVolume = soundEffectGain;

            PlayerPrefs.SetFloat("SoundEffectVolume", soundEffectsVolume);
            PlayerPrefs.Save();
        }



        if (muted && !isMaster)
        {
            if (source.volume > 0.0f)
                source.volume = 0.0f;
        }

        if (!muted && !isMaster)
        {
            if (source.volume != volume)
                source.volume = volume;
        }
    }





    public void setMuted(bool val)
    {
        if (isMaster)
        {
            masterMute = val;
        }
    }

    public void setMusicGain(float val)
    {
        if (isMaster)
        {
            if (val > 0.0f)
            { // volume up
                musicGain = musicGain < 1.0f ? musicGain + val : 1.0f;
            }
            else
            { //Volume Down
                musicGain = musicGain > 0.0f ? musicGain + val : 0.0f;
            }
            musicGain = Mathf.RoundToInt(musicGain * 10) * 0.1f;
        }
    }

    public void setSoundEffectGain(float val)
    {
        if (isMaster)
        {
            if (val > 0.0f)
            { // volume up
                soundEffectGain = soundEffectGain < 1.0f ? soundEffectGain + val : 1.0f;
            }
            else
            { //Volume Down
                soundEffectGain = soundEffectGain > 0.0f ? soundEffectGain + val : 0.0f;
            }
            soundEffectGain = Mathf.RoundToInt(soundEffectGain * 10) * 0.1f;
        }
    }
}