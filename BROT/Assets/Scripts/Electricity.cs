using UnityEngine;
/*
*@authors: Kai
*/
public class Electricity : MonoBehaviour
{
    int spriteWidth = 96;
    Vector3 startPos;
    Vector3 destPos;
    float offset = 0.25f;
    float scaleFac = 1.0f;


    GameObject fencePosts_A;
    GameObject fencePosts_B;


    public void setFencePosts(GameObject A, GameObject B)
    {
        fencePosts_A = A;
        fencePosts_B = B;
        this.SetPositions(A.transform, B.transform);
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public GameObject getFencePosts(int fence)
    {
        if (fence == 0)
            return fencePosts_A;
        else
            return fencePosts_B;
    }

    public int checkDistance(float dist, GameObject fencePost)
    {

        float distance = Vector3.Distance(fencePosts_A.transform.position, fencePosts_B.transform.position);
        if (distance < dist)
        {
            this.SetPositions(fencePosts_A.transform, fencePosts_B.transform);

            return 1;
        }
        else
        {
            return -1;
        }
    }
    public void setPositionsWithself()
    {
        SetPositions(fencePosts_A.transform, fencePosts_B.transform);
    }
    public void SetPositions(Transform start, Transform dest)
    {
        this.startPos = new Vector3(start.position.x, start.position.y + offset, start.position.z - 1);
        this.destPos = new Vector3(dest.position.x, dest.position.y + offset, dest.position.z - 1);

        scaleVoltageConnection();
    }

    void scaleVoltageConnection()
    {

        Vector3 centerPos = (startPos + destPos) / 2f;
        this.transform.position = centerPos;

        Vector3 direction = destPos - startPos;
        direction = Vector3.Normalize(direction);
        this.transform.right = direction;

        float distance = Vector3.Distance(startPos, destPos);

        scaleFac = (distance * 100) / spriteWidth;

        this.transform.localScale = new Vector3(scaleFac, 1, 1);
        //Debug.Log("ScaleFac: "+scaleFac +" | distance: "+distance);
        this.GetComponent<RectTransform>().sizeDelta = new Vector3(distance, 60f);
    }

}
