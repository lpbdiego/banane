using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricityPlaceMode : MonoBehaviour
{
    GameObject electricVisual;
    float fenceDistance = 2;
    private static List<GameObject> electricalIllusion = new List<GameObject>();
    private static List<GameObject> teslas = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        teslas = TeslaTower.getTesla();
        for (int i = 0; i < teslas.Count; i++)
        {
            if (teslas[i] != this.gameObject)
            {
                float distance = Vector3.Distance(this.transform.position, teslas[i].transform.position);
                //Debug.Log(distance);
                if (distance < fenceDistance)
                {
                    spawnNewElectricityVisual();
                    //fence_connections.Add(Instantiate(electricity, this.transform.position, this.transform.rotation * teslas[i].transform.rotation) as GameObject);
                    electricalIllusion[electricalIllusion.Count - 1].GetComponent<Electricity>().setFencePosts(this.gameObject, teslas[i]);
                    electricalIllusion[electricalIllusion.Count - 1].GetComponent<Electricity>().SetPositions(this.transform, teslas[i].transform);
                }
            }
        }


        /* for (int i = 0; i < teslas.Count; i++)
        {
            if (teslas[i] != this.gameObject)
            {
                float distance = Vector3.Distance(this.transform.position, teslas[i].transform.position);
                //Debug.Log(distance);
                if (distance < fenceDistance)
                {

                    if (electricalIllusion.Count == 0)
                    {
                        spawnNewElectricityVisual();
                        //fence_connections.Add(Instantiate(electricity, this.transform.position, this.transform.rotation * teslas[i].transform.rotation) as GameObject);
                        electricalIllusion[electricalIllusion.Count - 1].GetComponent<Electricity>().setFencePosts(this.gameObject, teslas[i]);
                        electricalIllusion[electricalIllusion.Count - 1].GetComponent<Electricity>().SetPositions(this.transform, teslas[i].transform);
                    }
                    else 
                    {
                        for (int k = 0; k < electricalIllusion.Count; k++)
                        {
                            GameObject A = electricalIllusion[k].GetComponent<Electricity>().getFencePosts(0);
                            GameObject B = electricalIllusion[k].GetComponent<Electricity>().getFencePosts(1);
                            if (electricalIllusion[k] != A && electricalIllusion[k] != B)
                            {

                                spawnNewElectricityVisual();
                                //fence_connections.Add(Instantiate(electricity, this.transform.position, this.transform.rotation * teslas[i].transform.rotation) as GameObject);
                                electricalIllusion[electricalIllusion.Count - 1].GetComponent<Electricity>().setFencePosts(this.gameObject, teslas[i]);
                                electricalIllusion[electricalIllusion.Count - 1].GetComponent<Electricity>().SetPositions(this.transform, teslas[i].transform);
                            }
                        }
                    }
                }
            }
        }*/
    }


    void spawnNewElectricityVisual()
    {
        GameObject tmp = Instantiate(Resources.Load<GameObject>("Prefabs/Structures/Electricity"), this.transform.position, this.transform.rotation);
        SpriteRenderer sr = tmp.GetComponent<SpriteRenderer>();
        sr.sprite = tmp.GetComponentInChildren<SpriteRenderer>().sprite;
        sr.color = new Color(1f, 1f, 1f, 0.25f);
        tmp.gameObject.GetComponent<BoxCollider2D>().enabled = false;
        electricalIllusion.Add(tmp);
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < electricalIllusion.Count; i++)
        {
            electricalIllusion[i].transform.position = this.transform.position;
            electricalIllusion[i].GetComponent<Electricity>().SetPositions(this.transform, teslas[i].transform);
        }


        for (int k = 0; k < 3; k++)
        {
            for (int i = 0; i < electricalIllusion.Count; i++)
            {

                GameObject A = electricalIllusion[i].GetComponent<Electricity>().getFencePosts(0);
                GameObject B = electricalIllusion[i].GetComponent<Electricity>().getFencePosts(1);
                float distance;
                if (A == this.gameObject || B == this.gameObject)
                {
                    if (A == this.gameObject)
                        distance = Vector3.Distance(this.transform.position, B.transform.position);
                    else
                        distance = Vector3.Distance(A.transform.position, this.transform.position);


                    if (distance < fenceDistance)
                    {
                        electricalIllusion[i].GetComponent<Electricity>().setPositionsWithself();
                    }
                    else
                    {
                        GameObject tmp = electricalIllusion[i];
                        electricalIllusion.Remove(tmp);
                        Destroy(tmp);
                    }

                }
            }
        }



        List<GameObject[]> fence_connections_Posts = new();
        for (int b = 0; b < electricalIllusion.Count; b++)
        {
            fence_connections_Posts.Add(new GameObject[] { electricalIllusion[b].GetComponent<Electricity>().getFencePosts(0), electricalIllusion[b].GetComponent<Electricity>().getFencePosts(1) });
        }

        for (int a = 0; a < teslas.Count; a++)
        {
            bool isConnected = false;
            for (int c = 0; c < fence_connections_Posts.Count; c++)
            {
                if ((this.gameObject == fence_connections_Posts[c][0] && teslas[a] == fence_connections_Posts[c][1]) || (this.gameObject == fence_connections_Posts[c][1] && teslas[a] == fence_connections_Posts[c][0]))
                {
                    isConnected = true;
                    break;
                }
            }

            if (!isConnected)
            {
                if (this.gameObject != teslas[a])
                {
                    float distance = Vector3.Distance(this.transform.position, teslas[a].transform.position);

                    if (distance < fenceDistance)
                    {
                        GameObject tmp = Instantiate(Resources.Load<GameObject>("Prefabs/Structures/Electricity"), this.transform.position, this.transform.rotation * teslas[a].transform.rotation);
                        
                        electricalIllusion.Add(tmp);
                        electricalIllusion[electricalIllusion.Count - 1].GetComponent<Electricity>().setFencePosts(this.gameObject, teslas[a]);
                        electricalIllusion[electricalIllusion.Count - 1].GetComponent<Electricity>().SetPositions(this.transform, teslas[a].transform);
                                SpriteRenderer sr = tmp.GetComponent<SpriteRenderer>();
                        sr.sprite = tmp.GetComponentInChildren<SpriteRenderer>().sprite;
                        sr.color = new Color(1f, 1f, 1f, 0.25f);
                        tmp.gameObject.GetComponent<BoxCollider2D>().enabled = false;
                    }

                }
            }
        }


    }

    void OnDestroy()
    {
        for (int i = 0; i < electricalIllusion.Count; i++)
        {
            Destroy(electricalIllusion[i].gameObject);
        }
        electricalIllusion.Clear();
    }
}
