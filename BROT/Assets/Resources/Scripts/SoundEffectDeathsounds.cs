using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;
using UnityEngine.Rendering;

public class SoundEffectDeathsounds : MasterVolume
{
    public bool deathSound = false;
    public int state = 0;


    public AudioClip[] clips = new AudioClip[7];
    private AudioClip clip;


    public enum SOUND_EFFECT_DEATHS
    {
        BULLET,
        ENEMY1,
        ENEMY2,
        ENEMY3,
        NEXUS,
        ENEMY_SUICIDE,
        PLAYER
    }

    new void Start() {
        base.Start();
        changeVolume(volumeGain);
    }

    
    public SOUND_EFFECT_DEATHS selected_effect = SOUND_EFFECT_DEATHS.PLAYER;

    
    AudioClip getCurrentClip() {
        //Debug.Log(clips[0]);
        switch(selected_effect) {
            case SOUND_EFFECT_DEATHS.BULLET: return clips[0];
            case SOUND_EFFECT_DEATHS.ENEMY1: return clips[1];
            case SOUND_EFFECT_DEATHS.ENEMY2: return clips[2];
            case SOUND_EFFECT_DEATHS.ENEMY3: return clips[3];
            case SOUND_EFFECT_DEATHS.NEXUS: return clips[4];
            case SOUND_EFFECT_DEATHS.ENEMY_SUICIDE: return clips[5];
            case SOUND_EFFECT_DEATHS.PLAYER: return clips[6];
            default: return clips[5];
        }
    }

    public void SetCurrentSound(SOUND_EFFECT_DEATHS snd) {
        selected_effect = snd;
        clip = getCurrentClip();
    }

    public void MakeDeathSound(SOUND_EFFECT_DEATHS snd, float vol) {
        GameObject tmpA  =  Instantiate(Resources.Load<GameObject>("Prefabs/Audio"), transform.position, Quaternion.Inverse(transform.rotation));
        tmpA.GetComponent<SoundEffectDeathsounds>().setDeathSound(true);
        tmpA.GetComponent<SoundEffectDeathsounds>().SetCurrentSound(snd);
        tmpA.GetComponent<SoundEffectDeathsounds>().changeVolume(vol);
        tmpA.GetComponent<SoundEffectDeathsounds>().triggerSound();
    }

    void setClip(AudioClip clip) {
        this.clip = clip;
    }




    public void changeVolume(float vol) {
        if(masterVolume == 0.0f)
            source.volume = 0.0f;
        else
            source.volume = vol;
    }

    public void triggerSound() {
        if(!source.isPlaying && deathSound) {
            //Debug.Log(selected_effect);
            source.PlayOneShot(getCurrentClip());
            state = 1;
        } else {
            
            source.PlayOneShot(getCurrentClip());
        }
    }


    public void setLoop(bool boo) {
        source.loop = boo;
    }

    public void stop() {
        source.Stop();
    }

    new void Update() {
        base.Update();
        if(volume != soundEffectsVolume) {
            volume = soundEffectsVolume;
            changeVolume(volume);
        }


        if(deathSound && !source.isPlaying && state == 1) {
            Destroy(this.gameObject);
        }
    }

    public void setDeathSound(bool boo) {deathSound = boo;}

    public bool GetPlaying(){return source.isPlaying;}



}
