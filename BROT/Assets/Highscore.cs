using UnityEngine;

public class Highscore : MonoBehaviour
{
    private int personalBestWave = 0;

    // Start is called before the first frame update
    void Start()
    {
        // Load personal best wave from player prefs
        personalBestWave = PlayerPrefs.GetInt("PersonalBestWave", 0);
        Debug.Log("Personal Best Wave: " + personalBestWave);
    }

    // Update personal best wave if the current wave surpasses it
    public void UpdatePersonalBestWave(int currentWave)
    {
        if (currentWave > personalBestWave)
        {
            personalBestWave = currentWave;
            // Save personal best wave to player prefs
            PlayerPrefs.SetInt("PersonalBestWave", personalBestWave);
            PlayerPrefs.Save(); // Ensure the value is saved immediately
            Debug.Log("New Personal Best Wave: " + personalBestWave);
        }
    }
}
