using System.Buffers.Text;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class FlameThrower : Timer
{
    [Header("-------------")]
    [Space(5)]
    [Header("FlameThrower")]
    [Space(5)]
    public Transform shootPosition;
    public float projectileSpeed = 10f;
    GameObject flames;
    public float flameDistance;
    private GameObject timer;

    public Vector2 fireDirection;

    private bool flameThrowerActive = false;
    public int flameThrowerDuration = 5;
    public int flameThrowerCoolDown = 2;


    public Transform aimTransform;
    private GameObject currentTarget;
    FlameThrowerFlamingBeam flamingBeam;

    private float distanceToStartShooting;
    private BoxCollider2D cld;
    SoundEffect soundEffect;

    public List<GameObject> enemiesInRange = new List<GameObject>();

    //public int flamingIntervall = 2;
    //private GameObject flamingtimerObject;
    //private Timer flamingTimer;

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
        cld = gameObject.GetComponent<BoxCollider2D>();   
         soundEffect = GetComponentInChildren<SoundEffect>();

        //flamingtimerObject = new GameObject();
        //flamingtimerObject.AddComponent<Timer>();
        //flamingTimer = flamingtimerObject.GetComponent<Timer>();
        //flamingTimer.T_SetTimerDuration(flamingIntervall);
        distanceToStartShooting = flameDistance + 1.0f;
        flames = Resources.Load<GameObject>("prefabs/structures/FlameThrowerFlame");
        flamingBeam = gameObject.GetComponentInChildren<FlameThrowerFlamingBeam>();
        // timer = Instantiate(Resources.Load<GameObject>("Timer"));
        //timer.GetComponent<TimerScript2>().reInit(this.GetComponent<TimerScript2>());
        //timer.GetComponent();
        T_SetTimerDuration(flameThrowerCoolDown);
        T_Play();
    }



    // Update is called once per frame
    new void Update()
    {
        base.Update();

        AimTowards(shootPosition.transform.position);
        //Debug.Log(1.0f / Time.deltaTime+" Is thios fps? ");
        if (T_GetTimerState() == TIMER_STATE.STOP && !flameThrowerActive && currentTarget != null)
        {
            T_SetTimerDuration(flameThrowerDuration);
            T_Reset();
            flameThrowerActive = true;
            //flamingTimer.T_Play();
            T_Play();
        }

        if (T_GetTimerFinish() && flameThrowerActive)
        {
            //flamingTimer.T_Stop();
            //flamingTimer.T_Reset();
            T_SetTimerDuration(flameThrowerCoolDown);
            T_Reset();
            flameThrowerActive = false;
            T_Play();
        }
        UpdateTarget();

        if (currentTarget != null)
        {
            AimTowards(currentTarget.transform.position);
        }
    }
    public override void T_TICK_BEHAVIOUR()
    {
        if (flameThrowerActive)
        {
            if(currentTarget != null) {
                Shoot(currentTarget.transform.position);
                flamingBeam.setActive(true);
            }
        }
        else
        {
            flamingBeam.setActive(false);
        }

    }



    void OnTriggerEnter2D(Collider2D collider)
    {


        if (collider.gameObject.CompareTag("Enemy"))
        {
            enemiesInRange.Add(collider.gameObject);
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("Enemy"))
        {
            enemiesInRange.Remove(collider.gameObject);
            if (currentTarget == collider.gameObject)
            {
                currentTarget = null;
            }
        }
    }

    void UpdateTarget()
    {
        if (currentTarget == null || !enemiesInRange.Contains(currentTarget))
        {
            float closestDistance = distanceToStartShooting;
            GameObject closestEnemy = null;

            foreach (GameObject enemy in enemiesInRange)
            {
                if(enemy != null) {
                    float distance = Vector2.Distance(transform.position, enemy.transform.position);
                    Debug.Log(enemy);
                    if (distance < closestDistance)
                    {
                        closestDistance = distance;
                        closestEnemy = enemy;
                    }
                }
            }

            currentTarget = closestEnemy;
        }
    }

    void AimTowards(Vector3 targetPosition)
    {
        Vector3 aimDirection = (targetPosition - transform.position).normalized;
        float angle = Mathf.Atan2(aimDirection.y, aimDirection.x) * Mathf.Rad2Deg;
        aimTransform.eulerAngles = new Vector3(0, 0, angle + 270); // Adjust this if your turret's orientation needs a different offset
    }


    private void Shootw(Vector3 targetPosition)
    {
        //Get Mouse Position
        //The mouse position is relative to the screen, we need its
        // location related to the world we are building in the game,
        // So we transform it using the cameras function ScreenToWorldPoint
        //Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        //Calculate Shoot Direction
        //fireDirection = (mousePosition - shootPosition.position).normalized;

        //Instantiate Bullet Prefab at the shooting position without rotating it. Quaternion.identity = no rotation

        //Apply Force to it

        //AimTowards(new Vector3(fireDirection.x, fireDirection.y, 0.0f));


        Vector3 shootDirection = (targetPosition - shootPosition.position).normalized;
        GameObject projectile = Instantiate(flames, shootPosition.position, Quaternion.identity);
        projectile.GetComponent<FlameThrowerFlame>().setByOwner(this.gameObject.transform, flameDistance);
        Rigidbody2D rb = projectile.GetComponent<Rigidbody2D>();
        rb.velocity = fireDirection.normalized * projectileSpeed;
        rb.transform.localScale = rb.transform.localScale;
        rb.AddForce(shootDirection * .01f, ForceMode2D.Impulse);



    }


    private void Shoot(Vector3 targetPosition)
    {
        if(!soundEffect.GetPlaying()) {
            soundEffect.SetCurrentSound(SoundEffect.SOUND_EFFECT.ATTACK);
            //soundEffect.changeVolume(.5f);
            soundEffect.triggerSound();
        }
        //Get Mouse Position
        //The mouse position is relative to the screen, we need its
        // location related to the world we are building in the game,
        // So we transform it using the cameras function ScreenToWorldPoint

        //Calculate Shoot Direction
        Vector2 fireDirection = (targetPosition - shootPosition.position).normalized;

        //Instantiate Bullet Prefab at the shooting position without rotating it. Quaternion.identity = no rotation
        GameObject projectile = Instantiate(flames, shootPosition.position, Quaternion.identity);
        projectile.GetComponent<FlameThrowerFlame>().setByOwner(this.gameObject.transform, flameDistance);

        //Apply Force to it
        Rigidbody2D rb = projectile.GetComponent<Rigidbody2D>();
        rb.velocity = fireDirection.normalized * projectileSpeed;
        rb.transform.localScale = rb.transform.localScale;

    }


    private void Shoot2(Vector3 targetPosition)
    {
        //Get Mouse Position
        //The mouse position is relative to the screen, we need its
        // location related to the world we are building in the game,
        // So we transform it using the cameras function ScreenToWorldPoint
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        //Calculate Shoot Direction
        Vector2 fireDirection = (mousePosition - shootPosition.position).normalized;

        //Instantiate Bullet Prefab at the shooting position without rotating it. Quaternion.identity = no rotation
        GameObject projectile = Instantiate(flames, shootPosition.position, Quaternion.identity);
        projectile.GetComponent<FlameThrowerFlame>().setByOwner(this.gameObject.transform, flameDistance);

        //Apply Force to it
        Rigidbody2D rb = projectile.GetComponent<Rigidbody2D>();
        rb.velocity = fireDirection.normalized * projectileSpeed;
        rb.transform.localScale = rb.transform.localScale;

    }

    public float getFlameDistance() { return flameDistance; }
}
