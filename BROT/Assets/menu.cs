using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class menu : MonoBehaviour
{

    private int personalBestWave = 0;

    public TMP_Text highscore;

    public GameObject howToOverlay;


    public void Start()
    {
        personalBestWave = PlayerPrefs.GetInt("PersonalBestWave", 0);
        highscore = GameObject.FindGameObjectWithTag("highscore").GetComponent<TMP_Text>();
        highscore.text = "Hi-Score: Wave " + personalBestWave.ToString();
        
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && howToOverlay.activeSelf)
                {
                    CloseHowToPlay();
                }
    }
    
    public void HowToPlay()
    {
       howToOverlay.SetActive(true);
    }

    public void CloseHowToPlay()
    {
        howToOverlay.SetActive(false);
    }


    public void PlayGame()
    {
        SceneManager.LoadScene(1);
    }

    public void QuitGame()
    {
            Application.Quit();
    }


}