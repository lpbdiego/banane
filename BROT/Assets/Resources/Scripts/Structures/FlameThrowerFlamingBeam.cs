using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class FlameThrowerFlamingBeam : Timer
{
    FlameThrower flameThrower;
    float dist;
    CapsuleCollider2D cld;
    bool active = false, beamActive = false;
    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
        flameThrower = gameObject.GetComponentInParent<FlameThrower>();
        
        dist = flameThrower.getFlameDistance();
        cld = gameObject.GetComponent<CapsuleCollider2D>();
        gameObject.transform.localScale = new Vector3(1.0f,dist,1.0f);
        gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
        T_Stop();
    }

    public override void T_TICK_BEHAVIOUR()
    {
        beamActive = !beamActive;
        toggleBeam(beamActive);
    }

    public void setActive(bool boo) {
        active = boo;
    }
    new void Update() {

        base.Update();
        if(active && T_GetTimerState() == TIMER_STATE.STOP) {
            T_Reset();
            T_Play();
        } else if(!active){
            T_Pause();
        } else if(active && T_GetTimerState() == TIMER_STATE.PAUSE) {
            T_Stop();
            T_Reset();
            T_Play();
        }

    }


    public void toggleBeam(bool boo) {
        cld.enabled = boo;
        //Debug.Log(boo);
    }
   
}
