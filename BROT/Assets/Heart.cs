using UnityEngine;

public class Heart: MonoBehaviour
{
    public float speed = 5f; // Speed at which the scrap moves towards the player
    private Transform playerTransform; // To store the player's transform
    public float detectionRadius = 0.3f; // The radius within which the scrap starts moving towards the player

    private void Start()
    {
        // Find the player by tag and store the reference
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (player != null)
        {
            playerTransform = player.transform;
        }
    }

    private void Update()
    {
        if (playerTransform != null)
        {
            float distanceToPlayer = Vector3.Distance(transform.position, playerTransform.position);

            // Check if the scrap is within the detection radius
            if (distanceToPlayer <= detectionRadius)
            {
                // Calculate direction towards the player
                Vector3 directionToPlayer = (playerTransform.position - transform.position).normalized;

                // Move the scrap piece towards the player
                transform.position += directionToPlayer * speed * Time.deltaTime;
            }
        }
    }
}
