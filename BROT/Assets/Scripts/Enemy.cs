using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] float health, maxHealth = 3f;
    public Transform target;

    NavMeshAgent agent;

    public GameObject scrapPrefab;
    public GameObject heartPrefab;

    public SpriteRenderer damageColor;
    bool damageColorOn = false;
    float damageColorCounter = 0;
    SpriteRenderer[] allSpriteRenderers;
    SpriteRenderer enemySpriteRenderer = null;


    //Hierarchy of Targets
    public GameObject Nexus;
    public GameObject Player;
    public GameObject[] Structures;

    public bool ranged;

    float distanceToNexus;
    float distanceToPlayer;

    public float rangeNexus; //How close does it have to be so it starts chasing nexus
    public float rangePlayer; //How close does it have to be so it starts chasing Player

    //electric stun
    public bool stunned = false;
    public float stunTimer = 0;

    //Attack
    public float attackDistance;
    [SerializeField] private float attackCooldown = 1f; // Cooldown duration in seconds
    private float timeSinceLastAttack = 0f; // Time since last attack
    public GameObject attackPrefab;

    public GameObject gun;
    SoundEffect soundEffects;
    SoundEffectDeathsounds soundEffectDeaths;
    int flameDamage = 1;


    //Upon collision with another GameObject, this GameObject will reverse direction
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Electricity"))
        {
            stun();
        }
        if (other.gameObject.CompareTag("Bullet"))
        {
            TakeDamage(other.gameObject.GetComponent<AttackPoints>().getAttackPoints());
        }

        if (other.gameObject.CompareTag("Flamethrower"))
        {
            TakeDamage(flameDamage);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet") || collision.gameObject.CompareTag("Turret"))
        {
            TakeDamage(collision.gameObject.GetComponent<AttackPoints>().getAttackPoints());

            if (collision.gameObject.GetComponent<Bullet>().isBoosted() == true)
            {
                stun();
            }
        }


    }

    protected void Start()
    {
        //Setup agent for Pathfinding
        agent = GetComponent<NavMeshAgent>();
        agent.updateRotation = false;
        agent.updateUpAxis = false;
        health = maxHealth;

        //Player
        Player = GameObject.Find("Player");
        Nexus = GameObject.Find("Nexus");
        soundEffects = GetComponentInChildren<SoundEffect>();
        soundEffectDeaths = GetComponentInChildren<SoundEffectDeathsounds>();

        allSpriteRenderers = GetComponentsInChildren<SpriteRenderer>();

        foreach (var sr in allSpriteRenderers)
        {
            if (sr.gameObject.name == "EnemyVisuals")
            {
                enemySpriteRenderer = sr;
                break;
            }
        }
    }

    protected void Update()
    {
        if (target)
        {
            agent.SetDestination(target.position);

            // Determine the direction of movement
            Vector3 direction = (target.position - transform.position).normalized;

            // Check if the enemy is moving predominantly horizontally
            if (Mathf.Abs(direction.x) > Mathf.Epsilon)
            {
                // Flip the sprite by setting the scale to -1 if moving left, 1 if moving right
                transform.localScale = new Vector3(Mathf.Sign(direction.x), transform.localScale.y, transform.localScale.z);

                if (ranged)
                {
                    //Also Flip the Gun back so it doesn't bug
                    gun.transform.localScale = new Vector3(Mathf.Sign(direction.x), transform.localScale.y, transform.localScale.z);

                }
            }
        }

        CheckDistances();
        PickTargetLogic();

        // Update the time since the last attack
        if (timeSinceLastAttack < attackCooldown)
        {
            timeSinceLastAttack += Time.deltaTime;
            //Debug.Log("Time since last attack=" + timeSinceLastAttack);
        }



        if (damageColorOn == true)
        {

            damageColorCounter += Time.deltaTime * 400;

            if (damageColorCounter >= 0)
            {

                enemySpriteRenderer.color = new Color(1f, 1f - damageColorCounter * 0.007f, 1f - damageColorCounter * 0.007f, 1f);

            }

            if (damageColorCounter >= 50)
            {

                enemySpriteRenderer.color = new Color(1f, 0.65f + (damageColorCounter - 50) * 0.007f, 0.65f + (damageColorCounter - 50) * 0.007f, 1f);

            }

            if (damageColorCounter >= 100)
            {
                damageColorOn = false;
                damageColorCounter = 0;
            }
        }


    }
    public void FixedUpdate()
    {
        if (stunned == true)
        {
            stunTimer += 1;

            if (stunTimer >= 25)
            {
                stunTimer = 0;
                stunned = false;
                GetComponent<NavMeshAgent>().speed = 1.5f;

            }


        }

    }
    void CheckDistances()
    {
        distanceToNexus = CalculateDistance(this.gameObject, Nexus);
        distanceToPlayer = CalculateDistance(this.gameObject, Player);
        //float distanceToClosestStructure = CalculateDistance(this.gameObject, closestStructure);

        if (target)
        {
            // Check if next to the target and cooldown has elapsed before attacking
            if (CalculateDistance(this.gameObject, target.gameObject) < attackDistance && timeSinceLastAttack >= attackCooldown)
            {
                Attack();
                timeSinceLastAttack = 0f; // Reset the cooldown timer
            }
        }
    }

    void TakeDamage(int value)
    {

        health -= value;

        if (health <= 0)
        {
            soundEffectDeaths.MakeDeathSound(SoundEffectDeathsounds.SOUND_EFFECT_DEATHS.ENEMY1, .3f);
            Die();
        }
        else
        {
            soundEffects.SetCurrentSound(SoundEffect.SOUND_EFFECT.HIT);
            //soundEffects.changeVolume(.5f);
            soundEffects.triggerSound();
            damageColorOn = true;
        }
    }


    public virtual void Die()
    {
        // Death Effects

        //Drop Scrap and Heart



        if (UnityEngine.Random.Range(0, 2) == 1)
        {
            Instantiate(scrapPrefab, transform.position, Quaternion.identity);
        }

        if (UnityEngine.Random.Range(0, 75) == 1)
        {
            Instantiate(heartPrefab, transform.position, Quaternion.identity);
        }

        //Destroy Game Object
        Destroy(this.gameObject);
    }



    void PickTargetLogic()
    {
        if (distanceToNexus < rangeNexus)
        {
            target = Nexus.transform;
        }

        if (distanceToPlayer < rangePlayer && target != Nexus.transform)
        {
            target = Player.transform;
        }

        else
        {
            //Chase Strcutrues....
        }
    }
    float CalculateDistance(GameObject object1, GameObject object2)
    {
        return Vector3.Distance(object1.transform.position, object2.transform.position);
    }

    public virtual void Attack()
    {
        if (!ranged)
        {
            soundEffects.SetCurrentSound(SoundEffect.SOUND_EFFECT.ATTACK);
            soundEffects.triggerSound();

            //Instantiate Attack Affect
            GameObject tmp = Instantiate(attackPrefab, transform.position, Quaternion.Inverse(transform.rotation));
            tmp.AddComponent<AttackPoints>();
            tmp.GetComponent<AttackPoints>().setAttackPoints(this.GetComponent<AttackPoints>().getAttackPoints());

        }
        //Cooldown
    }

    void stun()
    {
        soundEffects.SetCurrentSound(SoundEffect.SOUND_EFFECT.STUN);
        soundEffects.triggerSound();
        stunned = true;
        GetComponent<NavMeshAgent>().speed = GetComponent<NavMeshAgent>().speed * 0.25f;

    }



}