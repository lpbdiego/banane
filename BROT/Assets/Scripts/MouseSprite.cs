using System;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class MouseSprite : MonoBehaviour
{


    public static List<GameObject> structs = new List<GameObject>();
    private StructureRangeDetection rangeDetect;
    public Collider2D structureCollider;
    public float structureDistanceRadius = 1.0f;
    float offsetX = 0.0f;
    float offsetY = 0.0f;
    string additionals = "";
    bool additionSet = false;

    public void setAdditionals(string additionals)
    {
        this.additionals = additionals;
        additionSet = true;
    }


    Vector3 screenPosition;
    Vector3 worldPosition;
        GameObject tmp = null; 

    public void Update()
    {
        var mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        screenPosition = Input.mousePosition;
        screenPosition.z = Camera.main.nearClipPlane + 1;
        worldPosition = Camera.main.ScreenToWorldPoint(screenPosition);
        transform.position = new Vector3(worldPosition.x + offsetX, worldPosition.y + offsetY, worldPosition.z);
        transform.position = new Vector3(transform.position.x, transform.position.y + 0.2f, 0.0f);

        if (tmp != null && tmp.GetComponentInChildren<StructureRangeDetection>().getCollider().bounds.IntersectRay(mouseRay))
        {

        }
        else
        {
            tmp = null;
            offsetX = 0.0f;
            offsetY = 0.0f;
            for (int i = 0; i < PlacingController.structs.Count; i++)
            {
                rangeDetect = PlacingController.structs[i].GetComponentInChildren<StructureRangeDetection>();
                structureCollider = rangeDetect.getCollider();
                mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (structureCollider.bounds.IntersectRay(mouseRay))
                {
                    tmp = PlacingController.structs[i];
                    break;
                }
            }
        }

        if (additionSet)
        {
            switch (additionals)
            {
                case "":; break;
                case "electricity": this.AddComponent<ElectricityPlaceMode>(); break;
                default:; break;
            }
            additionSet = false;
        }
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("StructureDetector"))
        {
            Vector2 structurePos = new Vector2(other.transform.position.x, other.transform.position.y);
            float distance = Vector2.Distance(worldPosition, structurePos);
            // Debug.Log("Distance: "+distance);
            if (distance < structureDistanceRadius)
            {
                offsetX = worldPosition.x < structurePos.x ? -structureDistanceRadius : +structureDistanceRadius;
                offsetY = worldPosition.y < structurePos.y ? -structureDistanceRadius : +structureDistanceRadius;
                //Debug.Log("HELLO FROM THE OTHER SIDE");
            }
        }
    }

    
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("StructureDetector"))
        {
            Vector2 structurePos = new Vector2(other.transform.position.x, other.transform.position.y);
            float distance = Vector2.Distance(worldPosition, structurePos);
            // Debug.Log("Distance: "+distance);
            if (distance < structureDistanceRadius)
            {
                offsetX = worldPosition.x < structurePos.x ? -structureDistanceRadius : +structureDistanceRadius;
                offsetY = worldPosition.y < structurePos.y ? -structureDistanceRadius : +structureDistanceRadius;
                //Debug.Log("HELLO FROM THE OTHER SIDE");
            }
        }
    }
}

